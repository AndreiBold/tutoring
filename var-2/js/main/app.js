function applyDiscount(vehicles, discount){
    return new Promise((resolve, reject) => {
        if(typeof(discount) !== 'number') {
            reject(new Error('Invalid discount'));
        }
        // vehicles.foreach(vehicle => {
        //     if(typeof vehicle.make !== 'string' || typeof vehicle.price !== 'number') {
        //         reject(new Error('Invalid array format'));
        //     }
        // });
        if(vehicles.find(vehicle => typeof vehicle.make !== 'string' || typeof vehicle.price !== 'number')){
            reject(new Error('Invalid array format'));
        }
        
        const min = Math.min(...vehicles.map(vehicle => vehicle.price));
        if(discount> 0.5 * min) {
            reject(new Error('Discount too big'));
        } else {
            resolve(vehicles.map(vehicle => {
                return {
                    make: vehicle.make,
                    price: vehicle.price - discount
                };
            }));
        }
    });
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;